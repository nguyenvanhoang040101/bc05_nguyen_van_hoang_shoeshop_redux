import React, { Component } from "react";
import { dataShoe } from "./dataShoe";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import Cart from "./Cart";

export default class Ex_ShoeShop_Redux extends Component {
  state = {
    shoeArr: [],
    detail: dataShoe[1],
    cart: [],
  };
  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    // kiểm tra
    let index = this.state.cart.findIndex((item) => {
      return item.id == shoe.id;
    });
    if (index == -1) {
      //th2 : chua co
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      // th1 : da co
      cloneCart[index].number++;
    }

    this.setState({ cart: cloneCart });
    // th1: nếu sp đã có trong giỏ hàng => tăng key number
    //  th2 : nếu chưa có => tạo mới và push
  };
  render() {
    return (
      <div className="container">
        <Cart cart={this.state.cart} />
        <ListShoe
          handleAddToCart={this.handleAddToCart}
          handleChangeDetail={this.handleChangeDetail}
          shoeArr={this.state.shoeArr}
        />
        <DetailShoe />
      </div>
    );
  }
}
