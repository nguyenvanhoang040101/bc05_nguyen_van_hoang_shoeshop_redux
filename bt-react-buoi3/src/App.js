import logo from "./logo.svg";
import "./App.css";
import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
import Demo_Mini_Redux from "./Demo_Mini_Redux/Demo_Mini_Redux";
import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      {/* <Ex_ShoeShop /> */}
      {/* <Demo_Mini_Redux /> */}
      <Ex_ShoeShop_Redux />
    </div>
  );
}

export default App;
